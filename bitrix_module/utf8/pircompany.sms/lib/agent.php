<?php
namespace Pircompany\Sms;

class Agent {
	
	function turnSms() {
		$ob = new \Pircompany\Sms\Sender();
		$ob->getTurnSms();
		return '\\Pircompany\\Sms\\Agent::turnSms();';
	}

	function statusSms() {
		$ob = new \Pircompany\Sms\Sender();
		$ob->getStatusSms();
		return '\\Pircompany\\Sms\\Agent::statusSms();';
	}
	
}