<?
$MESS["PIRCOMPANY_SMS_SENDFORM_TITLE"] = "Отправка смс";
$MESS["PIRCOMPANY_SMS_SENDFORM_ERR_REQ"] = "Не заполнены обязательные поля";
$MESS["PIRCOMPANY_SMS_SENDFORM_NOTICE_SEND"] = "Сообщение отправлено";
$MESS["PIRCOMPANY_SMS_SENDFORM_ERR_PHONE"] = "Неверный формат номера телефона";
$MESS["PIRCOMPANY_SMS_SENDFORM_PHONE"] = "Номер телефона";
$MESS["PIRCOMPANY_SMS_SENDFORM_MESS"] = "Сообщение";
$MESS["PIRCOMPANY_SMS_SENDFORM_DATE"] = "Дата и время отправки";
$MESS["PIRCOMPANY_SMS_SENDFORM_SEND"] = "Отправить смс";
$MESS["PIRCOMPANY_SMS_SENDFORM_TAB"] = "Форма отправки смс";
$MESS["PIRCOMPANY_SMS_SENDFORM_ERR_1"] = "Ошибка в параметрах";
$MESS["PIRCOMPANY_SMS_SENDFORM_ERR_2"] = "Неверный логин или пароль";
$MESS["PIRCOMPANY_SMS_SENDFORM_ERR_3"] = "Недостаточно средств на счете Клиента";
$MESS["PIRCOMPANY_SMS_SENDFORM_ERR_4"] = "IP-адрес временно заблокирован из-за частых ошибок в запросах, либо другая серверная ошибка на сервисе";
$MESS["PIRCOMPANY_SMS_SENDFORM_ERR_5"] = "Неверный формат даты";
$MESS["PIRCOMPANY_SMS_SENDFORM_ERR_6"] = "Сообщение запрещено (по тексту или по имени отправителя)";
$MESS["PIRCOMPANY_SMS_SENDFORM_ERR_7"] = "Неверный формат номера телефона";
$MESS["PIRCOMPANY_SMS_SENDFORM_ERR_8"] = "Сообщение на указанный номер не может быть доставлено";
$MESS["PIRCOMPANY_SMS_SENDFORM_ERR_9"] = "Отправка более одного одинакового запроса на передачу SMS-сообщения либо более пяти одинаковых запросов на получение стоимости сообщения в течение минуты";
$MESS["PIRCOMPANY_SMS_SENDFORM_ERR_9999"] = "Неизвестная ошибка";
$MESS["PIRCOMPANY_SMS_SENDFORM_ERR_9998"] = "Сервис недоступен";
$MESS["PIRCOMPANY_SMS_SENDFORM_PRIM"] = "Отправлено из формы";
$MESS["PIRCOMPANY_SMS_SENDFORM_TRANSLIT"] = "Отправить транслитом?";
$MESS["PIRCOMPANY_SMS_SENDFORM_SENDER"] = "Отправитель";
$MESS["PIRCOMPANY_SMS_SENDFORM_ORDERPHONE"] = "Найдено несколько телефонов в заказе";
$MESS["PIRCOMPANY_SMS_SENDFORM_ORDER"] = "Данное смс будет привязано к заказу с идентификатором: ";
?>