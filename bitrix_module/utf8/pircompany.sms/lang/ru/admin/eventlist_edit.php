<?
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM'] = 'Шаблон';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_ERROR_ID'] = 'Шаблон не найден';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_EDIT'] = 'Редактирование шаблона';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_ADD'] = 'Добавление нового шаблона';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_ADD_CERENCY'] = 'Добавить новый шаблон';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_SAVED'] = 'Шаблон сохранен';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_EVENT'] = 'Код события';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_EVENT_DEF'] = 'Выберите событие';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_ACTIVE'] = 'Активность';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_SITE_ID'] = 'ID сайта';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_NAME'] = 'Название шаблона';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_SENDER'] = 'Отправитель смс';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_SENDER_VIBER'] = 'Отправитель Viber';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_SENDER_DESC'] = 'оставьте пустым, <br/>если хотите использовать отправителя по умолчанию';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE'] = 'Шаблон смс';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER'] = 'Шаблон Viber';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VK'] = 'Шаблон Вконтакте';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VK_WARN'] = '<font style="color:red;">Шаблон должен вернуть сериализованную строку с параметрами для зарегистрированного шаблона на сервисе. 
Возможность данной настройки требует знания php!</font>, <br><br>например: <pre>&lt;?
echo serialize(array("Name" => "#PROPERTY_NAME#","Where" => "#PROPERTY_TOWN#"))
?&gt;
</pre>';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER2'] = 'Viber: ссылка кнопки';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER2_DESC'] = 'Ссылка кнопки. Необязательный параметр. При добавлении, обязательно нужно указать название кнопки';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER1'] = 'Viber: текст кнопки';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER1_DESC'] = 'Текст кнопки. Не более 19 символов. Необязательный параметр. При добавлении, обязательно нужно указать ссылку кнопки';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER3'] = 'Viber: изображение';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER3_DESC'] = 'Ссылка на изображение. Необязательный параметр. В случае добавления изображения, также необходимо наличие кнопки';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER4'] = 'Viber: Время жизни сообщения';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER4_DESC'] = 'За этот период, система делает попытки доставить сообщение абоненту. Если за указанный период не будет доставлено сообщение, оно профинализируется со статусом просрочено либо не доставлено. Необязательный параметр. Указывается в минутах, например "60" - что значит 1 час. Если не указано, по умолчанию будет "60"';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER5'] = 'Viber: Считать финальным доставленным VIBER сообщением статус "прочитано"';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER6'] = 'Viber: Приоритет отправки';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER7'] = 'Вконтакте: Приоритет отправки';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER8'] = 'Смс: Приоритет отправки';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER5_DESC'] = 'В системе VIBER существуют статусы как доставлено и прочитано. Если отмечена опция, то в VIBER сообщении, 
в котором фигурирует статус доставлено, но не прочитано, по истечению время жизни VIBER сообщения - будет попытка переотправки сообщения по другой системе (например через SMS), 
если в самом запросе на сервис фигурирует каскадная отправка (отправка viber + смс);';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER6_DESC'] = 'Является порядком сортировки отправки VIBER сообщения (от меньшего к большему). 
Может принимать значение от 0 до 9, где 0 - не отправлять VIBER сообщение;';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER7_DESC'] = 'Является порядком сортировки отправки сообщения в Вконтакте (от меньшего к большему). 
Может принимать значение от 0 до 9, где 0 - не отправлять сообщение Вконтакте;';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER8_DESC'] = 'Является порядком сортировки отправки смс сообщения (от меньшего к большему). 
Может принимать значение от 0 до 9, где 0 - не отправлять смс сообщение; Опция доступна только при использовании каскадной отправки;';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_DESC'] = 'можно использовать макросы и php код';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_PARAM_TITLE'] = 'дополнительные параметры';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_PARAM_TITLE_1'] = 'Смс сообщение';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_PARAM_TITLE_2'] = 'Viber сообщение';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_PARAM_TITLE_3'] = 'Cообщение Вконтакте';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VKTID'] = 'Ид шаблона Вконтакте';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VKTID_DESC'] = 'Идентификатор шаблона в системе. Можете посмотреть Идентификатор шаблона в личном кабинете;';
?>