<?
$MESS["PIRCOMPANY_SMS_OPT_TITLE"] = "Pir.Company: SMS уведомления";
$MESS["PIRCOMPANY_SMS_OPT_TAB1"] = "Настройки модуля";
$MESS["PIRCOMPANY_SMS_OPT_TAB1_T"] = "Настройки модуля";
$MESS["PIRCOMPANY_SMS_OPT_TAB2"] = "Права доступа";
$MESS["PIRCOMPANY_SMS_OPT_TAB2_T"] = "Права доступа";
$MESS["PIRCOMPANY_SMS_OPT_SHLUZ_MAIN"] = "Настройки подключения к смс-шлюзу";
$MESS["PIRCOMPANY_SMS_OPT_SHLUZ_MAIN2"] = "Настройки подключения к viber-шлюзу";
$MESS["PIRCOMPANY_SMS_OPT_SHLUZ_PARAMS"] = "Дополнительные настройки";
$MESS["PIRCOMPANY_SMS_OPT_LIMITSMS"] = "Лимит отправки смс<br/> за 1 запрос к сервису";
$MESS["PIRCOMPANY_SMS_OPT_LOGIN"] = "Логин";
$MESS["PIRCOMPANY_SMS_OPT_API"] = "Имя хоста для отправки запросов (Сервер API)";
$MESS["PIRCOMPANY_SMS_OPT_PASSW"] = "Пароль";
$MESS["PIRCOMPANY_SMS_OPT_SHLUZ_OTP"] = "Отправитель смс";
$MESS["PIRCOMPANY_SMS_OPT_SENDER"] = "Отправитель смс<br/> по умолчанию";
$MESS["PIRCOMPANY_SMS_OPT_SENDER2"] = "Отправитель Viber<br/> по умолчанию";
$MESS["PIRCOMPANY_SMS_OPT_DLY"] = "для сайта";
$MESS["PIRCOMPANY_SMS_OPT_SEND"] = "Сохранить параметры";
$MESS["PIRCOMPANY_SMS_OPT_ERROR1"] = "Не удалось подключиться к сервису рассылок.";
$MESS["PIRCOMPANY_SMS_OPT_OK1"] = "Успешное подключение к сервису";
$MESS["PIRCOMPANY_SMS_OPT_BALANCE"] = "Остаток";
$MESS["PIRCOMPANY_SMS_OPT_CURENCY"] = "руб.";
$MESS["PIRCOMPANY_SMS_OPT_SENDER_GETTITLE"] = "Доступные имена отправителей";
$MESS["PIRCOMPANY_SMS_OPT_SENDERSTATE_ORDER"] = "оформляется";
$MESS["PIRCOMPANY_SMS_OPT_SENDERSTATE_COMPLETED"] = "готов к использованию";
$MESS["PIRCOMPANY_SMS_OPT_SENDERSTATE_REJECTED"] = "отклонен";
$MESS["PIRCOMPANY_SMS_OPT_SHLUZ_ADDSENDER"] = "Добавить новое имя отправителя";
$MESS["PIRCOMPANY_SMS_OPT_EMPTY_SENDER"] = "не найдены имена отправителей";
$MESS["PIRCOMPANY_SMS_OPT_ANY_SENDER"] = "Вам доступен любой отправитель";



$MESS["PIRCOMPANY_SMS_REGISTER_MESS"] = '<p>Для начала работы с установленным модулем "Pir.Company: SMS-уведомления" необходимо 
пройти <a href="https://bitrix.pir.company/ru/reg.html" target="_blank">регистрацию</a> на сервисе Pir Company и дождаться активации аккаунта, 
которая происходит с понедельника по пятницу с 9.00 до 18.00 по московскому времени.</p>
<p>Стоимость смс с именем отправителя тарифицируется - 2,40 руб. <br>Стоимость смс без имени отправителя - 2.60 руб.</p><p>
Для использования в смс уникального имени отправителя необходимо согласование с вашим менеджером сервиса Pir Company.</p>
<p><b>Каскадная отправка.</b><br>
Алгоритм работы:<br>
1. Номер абонента проверяется на наличие мессенджера Viber.<br>
2. Если мессенджер Viber активен, отправляется Viber сообщение.<br>
3. Если мессенджер не подключен, абоненту отправляется sms-сообщение.<br><br>
Объем Viber-сообщения до 1 тыс. символов.<br>
Есть возможность использования картинок и кнопки активности.<br>
Cтоимость вайбер-сообщения 1,90 руб., стоимость смс-сообщения 2,40
Стоимость ВК-сообщения 0,50, стоимость смс-сообщения 2,40</p>
';

?>