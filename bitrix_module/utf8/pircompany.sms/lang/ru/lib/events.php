<?
$MESS['PIRCOMPANY_SMS_EVENTCODE_ISMS_NEWORDER'] = 'Новый заказ';
$MESS['PIRCOMPANY_SMS_EVENTCODE_ISMS_STATUSUPDATE'] = 'Обновление статуса';
$MESS['PIRCOMPANY_SMS_EVENTCODE_ISMS_PAYED'] = 'Оплата заказа';
$MESS['PIRCOMPANY_SMS_EVENTCODE_ISMS_BXEVENT'] = 'Отправка письма';
$MESS['PIRCOMPANY_SMS_EVENTCODE_TABCONTROL_NAME'] = 'Отправленные смс';
$MESS['PIRCOMPANY_SMS_EVENTCODE_SENDSMS'] = 'Отправить смс к данному заказу';

$MESS["PIRCOMPANY_SMS_LIST_STATUS_1"] = "ожидает отправки с сайта";
$MESS["PIRCOMPANY_SMS_LIST_STATUS_2"] = "передано на шлюз";
$MESS["PIRCOMPANY_SMS_LIST_STATUS_3"] = "передано оператору";
$MESS["PIRCOMPANY_SMS_LIST_STATUS_4"] = "доставлено";
$MESS["PIRCOMPANY_SMS_LIST_STATUS_5"] = "просрочено";
$MESS["PIRCOMPANY_SMS_LIST_STATUS_6"] = "ожидает отправки на шлюзе";
$MESS["PIRCOMPANY_SMS_LIST_STATUS_7"] = "невозможно доставить";
$MESS["PIRCOMPANY_SMS_LIST_STATUS_8"] = "неверный номер";
$MESS["PIRCOMPANY_SMS_LIST_STATUS_9"] = "запрещено на сервисе";
$MESS["PIRCOMPANY_SMS_LIST_STATUS_10"] = "недостаточно средств";
$MESS["PIRCOMPANY_SMS_LIST_STATUS_11"] = "недоступный номер";
$MESS["PIRCOMPANY_SMS_LIST_STATUS_12"] = "неизвестный статус";

?>