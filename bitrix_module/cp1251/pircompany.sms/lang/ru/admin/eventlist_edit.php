<?
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM'] = '������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_ERROR_ID'] = '������ �� ������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_EDIT'] = '�������������� �������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_ADD'] = '���������� ������ �������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_ADD_CERENCY'] = '�������� ����� ������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_SAVED'] = '������ ��������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_EVENT'] = '��� �������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_EVENT_DEF'] = '�������� �������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_ACTIVE'] = '����������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_SITE_ID'] = 'ID �����';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_NAME'] = '�������� �������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_SENDER'] = '����������� ���';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_SENDER_VIBER'] = '����������� Viber';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_SENDER_DESC'] = '�������� ������, <br/>���� ������ ������������ ����������� �� ���������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE'] = '������ ���';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER'] = '������ Viber';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VK'] = '������ ���������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VK_WARN'] = '<font style="color:red;">������ ������ ������� ��������������� ������ � ����������� ��� ������������������� ������� �� �������. 
����������� ������ ��������� ������� ������ php!</font>, <br><br>��������: <pre>&lt;?
echo serialize(array("Name" => "#PROPERTY_NAME#","Where" => "#PROPERTY_TOWN#"))
?&gt;
</pre>';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER2'] = 'Viber: ������ ������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER2_DESC'] = '������ ������. �������������� ��������. ��� ����������, ����������� ����� ������� �������� ������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER1'] = 'Viber: ����� ������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER1_DESC'] = '����� ������. �� ����� 19 ��������. �������������� ��������. ��� ����������, ����������� ����� ������� ������ ������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER3'] = 'Viber: �����������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER3_DESC'] = '������ �� �����������. �������������� ��������. � ������ ���������� �����������, ����� ���������� ������� ������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER4'] = 'Viber: ����� ����� ���������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER4_DESC'] = '�� ���� ������, ������� ������ ������� ��������� ��������� ��������. ���� �� ��������� ������ �� ����� ���������� ���������, ��� ����������������� �� �������� ���������� ���� �� ����������. �������������� ��������. ����������� � �������, �������� "60" - ��� ������ 1 ���. ���� �� �������, �� ��������� ����� "60"';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER5'] = 'Viber: ������� ��������� ������������ VIBER ���������� ������ "���������"';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER6'] = 'Viber: ��������� ��������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER7'] = '���������: ��������� ��������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER8'] = '���: ��������� ��������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER5_DESC'] = '� ������� VIBER ���������� ������� ��� ���������� � ���������. ���� �������� �����, �� � VIBER ���������, 
� ������� ���������� ������ ����������, �� �� ���������, �� ��������� ����� ����� VIBER ��������� - ����� ������� ������������ ��������� �� ������ ������� (�������� ����� SMS), 
���� � ����� ������� �� ������ ���������� ��������� �������� (�������� viber + ���);';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER6_DESC'] = '�������� �������� ���������� �������� VIBER ��������� (�� �������� � ��������). 
����� ��������� �������� �� 0 �� 9, ��� 0 - �� ���������� VIBER ���������;';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER7_DESC'] = '�������� �������� ���������� �������� ��������� � ��������� (�� �������� � ��������). 
����� ��������� �������� �� 0 �� 9, ��� 0 - �� ���������� ��������� ���������;';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VIBER8_DESC'] = '�������� �������� ���������� �������� ��� ��������� (�� �������� � ��������). 
����� ��������� �������� �� 0 �� 9, ��� 0 - �� ���������� ��� ���������; ����� �������� ������ ��� ������������� ��������� ��������;';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_DESC'] = '����� ������������ ������� � php ���';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_PARAM_TITLE'] = '�������������� ���������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_PARAM_TITLE_1'] = '��� ���������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_PARAM_TITLE_2'] = 'Viber ���������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_PARAM_TITLE_3'] = 'C�������� ���������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VKTID'] = '�� ������� ���������';
$MESS['PIRCOMPANY_SMS_EVENTLIST_ADMIN_PARAM_TEMPLATE_VKTID_DESC'] = '������������� ������� � �������. ������ ���������� ������������� ������� � ������ ��������;';
?>