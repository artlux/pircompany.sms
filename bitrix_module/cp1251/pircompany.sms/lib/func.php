<?php
namespace Pircompany\Sms;

class Func {
	
	public static function formatTemplate($mess){
		
		if(strpos($mess,'|||')!==false){
			
			$finText = '';
			$tm = explode('|||',$mess);
			$viberText = preg_replace("/\[\[\[(.*)\]\]\]/is","",$tm[0]);
			
			$mess_viber_params = '';
			if(preg_match("/\[\[\[(.*)\]\]\]/is",$tm[0],$match)){
				$mess_viber_params = unserialize($match[1]);
			}
			
			if($viberText) {
				$finText .= '<b>viber</b>:<br> '.$viberText;
				if($mess_viber_params['IMAGE']) $finText .= '<br><img src="'.$mess_viber_params['IMAGE'].'" style="width:50px;height:auto;"/>';
			}
			
			if($mess_viber_params['LINK'] && $mess_viber_params['TEXT']) $finText .= '<br><a target="_blank" href="'.$mess_viber_params['LINK'].'">'.$mess_viber_params['TEXT'].'</a>';
			
			if($tm[1]) {
				if($finText) $finText .= '<br><br>';
				$finText .= '<b>sms</b>:<br> '.$tm[1];
			}
			if($tm[2]) {
				$temp = unserialize($tm[2]);
				if(!empty($temp)){
					if($tm[1]){
						$finText .= '<br><br>';
					}
					$finText .= '<b>vk</b>:<br> ';
					foreach($temp as $k=>$v){
						$finText .= $k.' => '.$v.'; ';
					}
				}
			}
			
			return $finText;
		}
		return $mess;
	}
	
}