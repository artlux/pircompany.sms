<?
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$aMenu = array();

$POST_RIGHT = $APPLICATION->GetGroupRight("pircompany.sms");
if ($POST_RIGHT == "D") $aMenu;

$aMenu = Array(
	"parent_menu" => "global_menu_marketing",
		"section" => "pircompany.sms",
		"sort" => 100,
		"module_id" => "pircompany.sms",
		"text" => Loc::getMessage("PIRCOMPANY_SMS_MODULE_NAME"),
		"title" => Loc::getMessage("PIRCOMPANY_SMS_MODULE_DESC"),
		"items_id" => "menu_pircompanysms",
		"items" => array(
			array(
				"text" => Loc::getMessage("PIRCOMPANY_SMS_MODULE_MENU_SENSMS"),
				"url" => "pircompany_sms_sendform.php?lang=".LANGUAGE_ID,
				"more_url" => Array(),
				"title" => Loc::getMessage("PIRCOMPANY_SMS_MODULE_MENU_SENSMS")
			),
			array(
				"text" => Loc::getMessage("PIRCOMPANY_SMS_MODULE_MENU_BALANCE"),
				"url" => "pircompany_sms_balance.php?lang=".LANGUAGE_ID,
				"more_url" => Array(),
				"title" => Loc::getMessage("PIRCOMPANY_SMS_MODULE_MENU_BALANCE")
			),
			array(
				"text" => Loc::getMessage("PIRCOMPANY_SMS_MODULE_MENU_HISTORY"),
				"url" => "pircompany_sms_list.php?lang=".LANGUAGE_ID,
				"more_url" => Array(),
				"title" => Loc::getMessage("MLIFESS_MENU_HISTORY")
			),
			array(
				"text" => Loc::getMessage("PIRCOMPANY_SMS_MODULE_MENU_EVENTLIST"),
				"url" => "pircompany_sms_eventlist.php?lang=".LANGUAGE_ID,
				"more_url" => Array('pircompany_sms_eventlist_edit.php?lang='.LANGUAGE_ID),
				"title" => Loc::getMessage("MLIFESS_MENU_EVENTLIST")
			),
		)
);
return $aMenu;
?>