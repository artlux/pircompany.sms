<?php
namespace Pircompany\Sms\Transport;

class Main{
	
	private $config;
	
	//конструктор, получаем данные доступа к шлюзу
	function __construct($params) {
		$this->config = $this->getConfig($params);
	}
	
	public function _getAllSender() {
		
		$xml = '<?xml version="1.0" encoding="utf-8" ?>
		<request>
		<security>
			<login value="'.$this->config->login.'" />
			<password value="'.$this->config->passw.'" />
		</security>
		</request>';
		
		$url = 'https://'.$this->config->server.'/xml/originator.php';
		$response = $this->openHttp($url, $xml);
		
		
		
		$data = new \stdClass();
		
		if(!$response){
			$data = new \stdClass();
			$data->error = 'Service is not available';
			$data->error_code = '9998';
			return $data;
		}
		
		$error = $this->checkError($response);
		if($error) {
			$data->error = $error;
			$data->error_code = $this->chechErrorCode($error);
			return $data;
		}
		
		$count_resp = preg_match_all('/<originator state="([A-z]+)">(.*)<\/originator>/Ui',$response, $matches);
		$count_resp2 = preg_match_all('/<any_originator>(.*)<\/any_originator>/Ui',$response, $matches2);
		
		//echo'<pre>';print_r($matches2);echo'</pre>';
		
		if($count_resp2>0 && $matches2[1][0] == 'TRUE'){
			$data->error = 'any originator avalible';
			$data->error_code = '9995';
			return $data;
		}elseif($count_resp>0 && is_array($matches[2])) {
			foreach($matches[2] as $k=>$sender) {
				$ob = new \stdClass();
				$ob->sender = $sender;
				$ob->state = $matches[1][$k];
				$arr[] = $ob;
			}
			$data = $arr;
		}else{
			$data->error = 'Error';
			$data->error_code = '9999';
			return $data;
		}
		
		return $data;
	}
	
	public function _sendSms ($phones, $mess, $time=0, $sender=false) {
		
		
		if($sender){
				if(strpos($sender,'|||')!==false){
					
				}else{
					$sender = \Bitrix\Main\Config\Option::get("pircompany.sms","sender_viber","","").'|||'.$sender;
			}
				}
		
		$data = new \stdClass();

		$phones = preg_replace("/[^0-9A-Za-z]/", "", $phones);
		
		$mess_viber = '';
		$mess_viber_params = array();
		if(strpos($mess,'|||')!==false){
			$tempMess = explode('|||',$mess);
			$mess = $tempMess[1];
			$mess_viber = $tempMess[0];
			$mess_viber_params = '';
			if(preg_match("/\[\[\[(.*)\]\]\]/is",$mess_viber,$match)){
				$mess_viber_params = unserialize($match[1]);
			}
			$mess_viber = preg_replace("/\[\[\[(.*)\]\]\]/is","",$mess_viber);
			$mess_vk = unserialize($tempMess[2]);
		}
		
		if(toLower(SITE_CHARSET)=='windows-1251') {
			$mess = $GLOBALS['APPLICATION']->ConvertCharset($mess, SITE_CHARSET, 'UTF-8');
			$mess_viber = $GLOBALS['APPLICATION']->ConvertCharset($mess_viber, SITE_CHARSET, 'UTF-8');
			$mess_vk = $GLOBALS['APPLICATION']->ConvertCharsetArray($mess_vk, SITE_CHARSET, 'UTF-8');
			if($mess_viber_params['TEXT']) $mess_viber_params['TEXT'] = $GLOBALS['APPLICATION']->ConvertCharset($mess_viber_params['TEXT'], SITE_CHARSET, 'UTF-8');
		}
		if(!$sender) {
			$sender = $this->config->sender;
		}
		$temp_sender = explode('|||',$sender);
		$sender = $temp_sender[1];
		$sender_viber = $temp_sender[0];
		
		$mess_viber_params_copy = $mess_viber_params;
		if(!$mess_viber_params['PRIOR_V']){
			unset($mess_viber_params);
			unset($mess_viber);
			unset($sender_viber);
		}
		if(!$mess_viber_params['PRIOR_VK']){
			unset($mess_vk);
		}
		
		//print_r($mess_viber_params);die();
		
		if($mess_viber || !empty($mess_vk)){
			
			$arSendParams = array(
				'login' => $this->config->login,
				'password' => $this->config->passw,
				'message' => array(
					array(
						'phones' => array(
							array('phone' => $phones)
						)
					)
				)
			);
			
			if($sender_viber) $arSendParams['message'][0]['sender_viber'] = $sender_viber;
			if($mess_viber) $arSendParams['message'][0]['text_viber'] = $mess_viber;
			if($mess_viber_params['PRIOR_V']) $arSendParams['message'][0]['send_viber'] = $mess_viber_params['PRIOR_V'];
			if($mess_viber_params['LINK']) $arSendParams['message'][0]['button_url'] = $mess_viber_params['LINK'];
			if($mess_viber_params['TEXT']) $arSendParams['message'][0]['button_text'] = $mess_viber_params['TEXT'];
			if($mess_viber_params['IMAGE']) $arSendParams['message'][0]['image_url'] = $mess_viber_params['IMAGE'];
			if($mess_viber_params['PERIOD']) $arSendParams['message'][0]['validity_period_viber'] = $mess_viber_params['PERIOD'];
			if($mess_viber_params['STATUS']) $arSendParams['message'][0]['finale_viber_read'] = $mess_viber_params['STATUS'];
			
			if($mess){
				$arSendParams['message'][0]['send_sms'] = $mess_viber_params_copy['PRIOR_S'];
				$arSendParams['message'][0]['sender_sms'] = $sender;
				$arSendParams['message'][0]['text_sms'] = $mess;
			}
			
			//сообщение в вк
			if(!empty($mess_vk)){
				$arSendParams['message'][0]['send_vk'] = $mess_viber_params_copy['PRIOR_VK'];
				$arSendParams['message'][0]['templateId'] = $mess_viber_params_copy['VKTID'];
				$arSendParams['message'][0]['templateData'] = $mess_vk;
			}
			
			/*echo'<pre>';print_r($mess_vk);echo'</pre>';
			echo'<pre>';print_r($mess_viber_params);echo'</pre>';
			echo'<pre>';print_r($arSendParams);echo'</pre>';*/
			//die();
			
			$url = 'https://'.$this->config->server_viber.'/sendmessagejson.php';
			$json = json_encode($arSendParams);
			$response = $this->openHttp($url, $json, true);
			$response = json_decode($response);
			
			if(!$response){
				$data = new \stdClass();
				$data->error = 'Service is not available';
				$data->error_code = '9998';
				//return $data;
			}
			if($response->error){
				$data->error = $GLOBALS['APPLICATION']->ConvertCharset($response->error, 'UTF-8', SITE_CHARSET);
				$data->error_code = $this->chechErrorCode($response->error);
				return $data;
			}
			if($response->message[0]->error){
				$data->error = $GLOBALS['APPLICATION']->ConvertCharset($response->message[0]->error, 'UTF-8', SITE_CHARSET);
				$data->error_code = $this->chechErrorCode($response->message[0]->error);
				return $data;
			}
			if($response->message[0][0]->error){
				$data->error = $GLOBALS['APPLICATION']->ConvertCharset($response->message[0][0]->error, 'UTF-8', SITE_CHARSET);
				$data->error_code = $this->chechErrorCode($response->message[0][0]->error);
				return $data;
			}
			
			if($response->message[0][0]->id_message){
				$data->id = '_'.$response->message[0][0]->id_message;
				$data->cnt = $matches[2][0];
				$data->cost = '';
				$data->balance = '';
				return $data;
			}
			
			$data->error = 'Parse response error';
			$data->error_code = '9999';
			return $data;
			
			//echo'<pre>';print_r($response);echo'</pre>';
			//echo'<pre>';print_r($arSendParams);echo'</pre>';die();
			
		}else{
		
			$xml = '<?xml version="1.0" encoding="utf-8" ?>
			<request>
			<security>
				<login value="'.$this->config->login.'" />
				<password value="'.$this->config->passw.'" />
			</security>
			<message type="sms">
				<sender>'.$sender.'</sender>
				<text>'.$mess.'</text>
				<abonent phone="'.$phones.'"/>
			</message>
			</request>';
			
			$url = 'https://'.$this->config->server.'/xml/';
			
			$response = $this->openHttp($url, $xml);
			
			if(!$response){
				$data = new \stdClass();
				$data->error = 'Service is not available';
				$data->error_code = '9998';
				return $data;
			}
			
			$error = $this->checkError($response);
			if($error) {
				$data->error = $error;
				$data->error_code = $this->chechErrorCode($error);
				return $data;
			}
			
			$count_resp_err = preg_match_all('/<information number_sms="">(.*)<\/information>/Ui',$response, $matches_err);
			if($count_resp_err>0) {
				$err = $GLOBALS['APPLICATION']->ConvertCharset($matches_err[1][0], 'UTF-8', SITE_CHARSET);
				$data->error = $err;
				$data->error_code = $this->chechErrorCode($err);
				return $data;
			}
			
			$count_resp = preg_match_all('/<information.*id_sms="(.*)".*parts="(.*)">(.*)<\/information>/Ui',$response, $matches);
			
			if($count_resp>0){
				$data->id = $matches[1][0];
				$data->cnt = $matches[2][0];
				$data->cost = '';
				$data->balance = '';
				return $data;
			}
			else{
				$data->error = 'Parse response error';
				$data->error_code = '9999';
				return $data;
			}
		
		}
	
	}
	
	public function _getBalance () {
	
		$xml = '<?xml version="1.0" encoding="utf-8" ?>
		<request>
		<security>
			<login value="'.$this->config->login.'" />
			<password value="'.$this->config->passw.'" />
		</security>
		</request>';

		$url = 'https://'.$this->config->server.'/xml/balance.php';
		
		$response = $this->openHttp($url, $xml);
		
		$data = new \stdClass();
		
		if(!$response){
			$data = new \stdClass();
			$data->error = 'Service is not available';
			$data->error_code = '9998';
			return $data;
		}
		
		$error = $this->checkError($response);
		if($error) {
			$data->error = $error;
			$data->error_code = $this->chechErrorCode($error);
			return $data;
		}
		
		$count_resp = preg_match_all('/<money.*>(.*)<\/money>/Ui',$response, $matches);
		
		if($count_resp>0) {
			$data->balance = $matches[1][0];
		}else{
			$data->error = $error;
			$data->error_code = $this->chechErrorCode($error);
			return $data;
		}
		
		return $data;
		
	}
	
	//multiple sms
	public function _getStatusSms($arAll=array()){
		
		//проверка кастомной отправки
		$custom = false;
		$arNew = array();
		foreach($arAll as &$data){
			if(substr($data['SMSID'],0,1)=='_') {
				$data['SMSID'] = substr($data['SMSID'],1);
				$custom = true;
				$arNew[] = $data;
			}
		}
		unset($data);
		
		if($custom){
			$arAll = $arNew;
		}
		
		if(!$custom){
			$arLinkSmsId = array();
			
			$xml = '<?xml version="1.0" encoding="utf-8" ?>
			<request>
			<security>
				<login value="'.$this->config->login.'" />
				<password value="'.$this->config->passw.'" />
			</security>
			<get_state>';
			foreach($arAll as $data){
				$arLinkSmsId[$data['SMSID']] = $data['ID']; //для оптимизации все обновления по ид на стороне клиента
				$xml .=	'<id_sms>'.$data['SMSID'].'</id_sms>'."\n";
			}
			$xml .= '</get_state>
			</request>';
			
			$url = 'https://'.$this->config->server.'/xml/state.php';
			
			$response = $this->openHttp($url, $xml);
			//print_r($GLOBALS['APPLICATION']->ConvertCharset($response, 'UTF-8', SITE_CHARSET));
			
			$allResponce = array();
			
			if(!$response){
				foreach($arLinkSmsId as $serviceId=>$baseId){
					$data = new \stdClass();
					$data->error = 'Service is not available';
					$data->error_code = '9998';
					$data->baseId = $baseId;
					$allResponce[] = $data;
				}
				return $allResponce;
			}
			
			$error = $this->checkError($response);
			
			if($error) {
				foreach($arLinkSmsId as $serviceId=>$baseId){
				$data = new \stdClass();
				$data->error = $error;
				$data->error_code = $this->chechErrorCode($error);
				$data->baseId = $baseId;
				$allResponce[] = $data;
				}
				return $allResponce;
			}
			
			$count_resp = preg_match_all('/<state id_sms=(.*) time="(.*?)".*>(.*)<\/state>/Ui',$response, $matches);
			
			if($count_resp>0){
				//print_r(preg_replace("/([^0-9])/is","",$matches[1][0]));
				if(preg_replace("/([^0-9])/is","",$matches[1][0])){
				foreach($matches[2] as $key=>$m){
					if($this->_checkStatus($matches[3][$key])){
						$data = new \stdClass();
						$data->last_timestamp = strtotime($matches[2][$key]);
						$data->status = $this->_checkStatus($matches[3][$key]);
						$data->baseId = $arLinkSmsId[preg_replace("/([^0-9])/is","",$matches[1][$key])];
						$allResponce[] = $data;
					}else{
						$data = new \stdClass();
						$data->error = 'error - '.$GLOBALS['APPLICATION']->ConvertCharset($matches[3][$key], 'UTF-8', SITE_CHARSET);
						$data->error_code = '9998';
						$data->baseId = $arLinkSmsId[preg_replace("/([^0-9])/is","",$matches[1][$key])];
						$allResponce[] = $data;
					}
				}
				
				return $allResponce;
				}
			}
			
			foreach($arLinkSmsId as $serviceId=>$baseId){
				$data = new \stdClass();
				$data->error = 'Service is not available';
				$data->error_code = '9998';
				$data->baseId = $baseId;
				$allResponce[] = $data;
			}
			return $allResponce;
		
		}else{
			
			$arLinkSmsId = array();
			foreach($arAll as $data){
				$arLinkSmsId[$data['SMSID']] = $data['ID']; //для оптимизации все обновления по ид на стороне клиента
			}
			
			$url = 'https://'.$this->config->server_viber.'/sendmessagejson.php';
			
			$arSendParams = array(
				'login' => $this->config->login,
				'password' => $this->config->passw,
				'state' => array()
			);
			foreach($arAll as $data){
				$t = new \stdClass();
				$t->id_message = $data['SMSID'];
				$arSendParams['state'][] = $t;
			}
			
			$json = json_encode($arSendParams);
			$response = $this->openHttp($url, $json, true);
			
			$allResponce = array();
			
			if(!$response){
				foreach($arLinkSmsId as $serviceId=>$baseId){
					$data = new \stdClass();
					$data->error = 'Service is not available';
					$data->error_code = '9998';
					$data->baseId = $baseId;
					$allResponce[] = $data;
				}
				return $allResponce;
			}
			
			$response = json_decode($response);
			//print_r($response);
			//print_r($arSendParams);
			
			if($response->error){
				foreach($arLinkSmsId as $serviceId=>$baseId){
					$data = new \stdClass();
					$data->error = $GLOBALS['APPLICATION']->ConvertCharset($response->error, 'UTF-8', SITE_CHARSET);
					$data->error_code = '9998';
					$data->baseId = $baseId;
					$allResponce[] = $data;
				}
				return $allResponce;
			}
			if($response->message[0]->error){
				foreach($arLinkSmsId as $serviceId=>$baseId){
					$data = new \stdClass();
					$data->error = $GLOBALS['APPLICATION']->ConvertCharset($response->message[0]->error, 'UTF-8', SITE_CHARSET);
					$data->error_code = '9998';
					$data->baseId = $baseId;
					$allResponce[] = $data;
				}
				return $allResponce;
			}
			if($response->message[0][0]->error){
				foreach($arLinkSmsId as $serviceId=>$baseId){
					$data = new \stdClass();
					$data->error = $GLOBALS['APPLICATION']->ConvertCharset($response->message[0][0]->error, 'UTF-8', SITE_CHARSET);
					$data->error_code = '9998';
					$data->baseId = $baseId;
					$allResponce[] = $data;
				}
				return $allResponce;
			}
			
			$allResponce = array();
			foreach($response->state as $ob){
				if($this->_checkStatus($ob->state)){
					$data = new \stdClass();
					$data->last_timestamp = strtotime($ob->time_change_state);
					$data->status = $this->_checkStatus($ob->state);
					$data->status_mess = $ob->state_viber.','.$ob->state_sms.','.$ob->state_vk;
					$data->baseId = $arLinkSmsId[$ob->id_message];
					$allResponce[] = $data;
				}else{
					$data = new \stdClass();
					$data->error = 'error - '.$GLOBALS['APPLICATION']->ConvertCharset($ob->state, 'UTF-8', SITE_CHARSET);
					$data->error_code = '9998';
					$data->baseId = $arLinkSmsId[$ob->id_message];
					$allResponce[] = $data;
				}
			}
			return $allResponce;
			
			//echo'<pre>';print_r($response);echo'</pre>';
			
			foreach($arLinkSmsId as $serviceId=>$baseId){
				$data = new \stdClass();
				$data->error = 'Service is not available';
				$data->error_code = '9998';
				$data->baseId = $baseId;
				$allResponce[] = $data;
			}
			return $allResponce;
			
		}
	}
	
	//old method for get status
	public function _getStatusSms_old($smsid,$phone=false) {
		
		$xml = '<?xml version="1.0" encoding="utf-8" ?>
		<request>
		<security>
			<login value="'.$this->config->login.'" />
			<password value="'.$this->config->passw.'" />
		</security>
		<get_state>
			<id_sms>'.$smsid.'</id_sms>
		</get_state>
		</request>';
		
		$url = 'https://'.$this->config->server.'/xml/state.php';
		
		$response = $this->openHttp($url, $xml);

		$data = new \stdClass();
		
		if(!$response){
			$data->error = 'Service is not available';
			$data->error_code = '9998';
			return $data;
		}
		
		$error = $this->checkError($response);
		
		if($error) {
			$data->error = $error;
			$data->error_code = $this->chechErrorCode($error);
			return $data;
		}
		
		$count_resp = preg_match_all('/<state.*time="(.*)".*>(.*)<\/state>/Ui',$response, $matches);
		
		if($count_resp>0){
			if($this->_checkStatus($matches[2][0])){
			$data->last_timestamp = strtotime($matches[1][0]);
			$data->status = $this->_checkStatus($matches[2][0]);
			return $data;
			}
		}
			$data->error = 'Service is not available';
			$data->error_code = '9998';
			return $data;
		
	}
	
	private function getConfig($params) {
		
		$c = new \stdClass();
		if(strpos($params['login'],"||")!==false){
			$arPrm = explode("||",$params['login']);
		}else{
			$arPrm = array('bitrix.pir.company',$params['login']);
		}
		$c->login = $arPrm[1];
		$c->server = $arPrm[0];
		$c->passw = $params['passw'];
		$c->sender = $params['sender'];
		$c->server_viber = $params['api_viber'];
		
		return $c;
		
	}
	
	private function openHttp($url, $xml, $json=false) {
		
		//default bitrix HttpClient
		$curl = \Bitrix\Main\Config\Option::get("pircompany.sms","curl","");
		//print_r($url);die();
		if($curl != 'Y'){
		
			$httpClient = new \Bitrix\Main\Web\HttpClient();
			if($json){
				$httpClient->setHeader('Content-Type', 'application/json; charset=utf-8', true);
			}else{
				$httpClient->setHeader('Content-Type', 'text/xml; charset=utf-8', true);
			}
			$result = $httpClient->post($url, $xml);
			
			file_put_contents($_SERVER["DOCUMENT_ROOT"].'/sms_log.txt',print_r($result,true),FILE_APPEND);
			
			return $result;
			
		}
		
		if (!function_exists('curl_init')) {
		    return false;
		}

		$ch = curl_init();
		if($json){
			curl_setopt( $ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json; charset=utf-8' ) );
		}else{
			curl_setopt( $ch, CURLOPT_HTTPHEADER, array( 'Content-type: text/xml; charset=utf-8' ) );
		}
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_CRLF, true );
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $xml );
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, true );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, true );

		$result = curl_exec($ch);
		curl_close($ch);
		file_put_contents($_SERVER["DOCUMENT_ROOT"].'/sms_log.txt',print_r($result,true),FILE_APPEND);
		return $result;
		
	}
	
	private function checkError($resp) {
		$count = preg_match_all('/<error>(.*)<\/error>/Ui',$resp, $matches);
		if($count>0) {
			$error = $GLOBALS['APPLICATION']->ConvertCharset($matches[1][0], 'UTF-8', SITE_CHARSET);
			return $error;
		}
		return false;
	}
	
	private function chechErrorCode($code) {
		if(toLower(SITE_CHARSET)=='windows-1251') $code = $GLOBALS['APPLICATION']->ConvertCharset($code, SITE_CHARSET, 'UTF-8');
		$code = trim($code);
		if(strpos($code,'логин')!==false) return 2;
		if(strpos($code,'XML')!==false) return 1;
		if(strpos($code,'POST')!==false) return 1;
		
		if(strpos($code,'логин или')!==false) return 2; //'Неправильный логин или пароль'
		if(strpos($code,'формат XML')!==false) return 1; //'Неправильный формат XML документа'
		if(strpos($code,'аккаунт заблокирован')!==false) return 2; //'Ваш аккаунт заблокирован'
		if(strpos($code,'POST данные')!==false) return 1; //'POST данные отсутствуют'
		if(strpos($code,'закончились SMS')!==false) return 3; //'У нас закончились SMS. Для разрешения проблемы свяжитесь с менеджером.'
		if(strpos($code,'Закончились SMS')!==false) return 3; //'Закончились SMS.'
		if(strpos($code,'Аккаунт заблокирован')!==false) return 2; //'Аккаунт заблокирован.'
		if(strpos($code,'Укажите номер')!==false) return 1; //'Укажите номер телефона.'
		if(strpos($code,'стоп-листе')!==false) return 8; //'Номер телефона присутствует в стоп-листе.'
		if(strpos($code,'направление закрыто')!==false) return 6; //'Данное направление закрыто для вас.'
		if(strpos($code,'направление закрыто')!==false) return 6; //'Данное направление закрыто.'
		if(strpos($code,'SMS отклонен')!==false) return 6; //'Текст SMS отклонен модератором.'
		if(strpos($code,'Нет отправителя')!==false) return 6; //'Нет отправителя.'
		if(strpos($code,'символов для цифровых')!==false) return 6; //'Отправитель не должен превышать 15 символов для цифровых номеров и 11 символов для буквенно-числовых.'
		if(strpos($code,'телефона должен')!==false) return 7; //'Номер телефона должен быть меньше 15 символов.'
		if(strpos($code,'текста сообщения')!==false) return 1; //'Нет текста сообщения.'
		if(strpos($code,'Нет ссылки')!==false) return 1; //'Нет ссылки.'
		if(strpos($code,'название контакта')!==false) return 1; //'Укажите название контакта и хотя бы один параметр для визитной карточки.'
		if(strpos($code,'отправителя нет')!==false) return 6; //'Такого отправителя нет.'
		if(strpos($code,'не прошел модерацию')!==false) return 6; //'Отправитель не прошел модерацию.'
		
		return 9999;
	
	}
	
	private function _checkStatus($code) {
	
		if($code=='send') return 3;
		if($code=='not_deliver') return 7;
		if($code=='expired') return 5;
		if($code=='deliver') return 4;
		if($code=='partly_deliver') return false;
		return false;
		
	}
	
}
?>