<?
$MESS["PIRCOMPANY_SMS_OPT_TITLE"] = "Pir.Company: SMS �����������";
$MESS["PIRCOMPANY_SMS_OPT_TAB1"] = "��������� ������";
$MESS["PIRCOMPANY_SMS_OPT_TAB1_T"] = "��������� ������";
$MESS["PIRCOMPANY_SMS_OPT_TAB2"] = "����� �������";
$MESS["PIRCOMPANY_SMS_OPT_TAB2_T"] = "����� �������";
$MESS["PIRCOMPANY_SMS_OPT_SHLUZ_MAIN"] = "��������� ����������� � ���-�����";
$MESS["PIRCOMPANY_SMS_OPT_SHLUZ_MAIN2"] = "��������� ����������� � viber-�����";
$MESS["PIRCOMPANY_SMS_OPT_SHLUZ_PARAMS"] = "�������������� ���������";
$MESS["PIRCOMPANY_SMS_OPT_LIMITSMS"] = "����� �������� ���<br/> �� 1 ������ � �������";
$MESS["PIRCOMPANY_SMS_OPT_LOGIN"] = "�����";
$MESS["PIRCOMPANY_SMS_OPT_API"] = "��� ����� ��� �������� �������� (������ API)";
$MESS["PIRCOMPANY_SMS_OPT_PASSW"] = "������";
$MESS["PIRCOMPANY_SMS_OPT_SHLUZ_OTP"] = "����������� ���";
$MESS["PIRCOMPANY_SMS_OPT_SENDER"] = "����������� ���<br/> �� ���������";
$MESS["PIRCOMPANY_SMS_OPT_SENDER2"] = "����������� Viber<br/> �� ���������";
$MESS["PIRCOMPANY_SMS_OPT_DLY"] = "��� �����";
$MESS["PIRCOMPANY_SMS_OPT_SEND"] = "��������� ���������";
$MESS["PIRCOMPANY_SMS_OPT_ERROR1"] = "�� ������� ������������ � ������� ��������.";
$MESS["PIRCOMPANY_SMS_OPT_OK1"] = "�������� ����������� � �������";
$MESS["PIRCOMPANY_SMS_OPT_BALANCE"] = "�������";
$MESS["PIRCOMPANY_SMS_OPT_CURENCY"] = "���.";
$MESS["PIRCOMPANY_SMS_OPT_SENDER_GETTITLE"] = "��������� ����� ������������";
$MESS["PIRCOMPANY_SMS_OPT_SENDERSTATE_ORDER"] = "�����������";
$MESS["PIRCOMPANY_SMS_OPT_SENDERSTATE_COMPLETED"] = "����� � �������������";
$MESS["PIRCOMPANY_SMS_OPT_SENDERSTATE_REJECTED"] = "��������";
$MESS["PIRCOMPANY_SMS_OPT_SHLUZ_ADDSENDER"] = "�������� ����� ��� �����������";
$MESS["PIRCOMPANY_SMS_OPT_EMPTY_SENDER"] = "�� ������� ����� ������������";
$MESS["PIRCOMPANY_SMS_OPT_ANY_SENDER"] = "��� �������� ����� �����������";



$MESS["PIRCOMPANY_SMS_REGISTER_MESS"] = '<p>��� ������ ������ � ������������� ������� "Pir.Company: SMS-�����������" ���������� 
������ <a href="https://bitrix.pir.company/ru/reg.html" target="_blank">�����������</a> �� ������� Pir Company � ��������� ��������� ��������, 
������� ���������� � ������������ �� ������� � 9.00 �� 18.00 �� ����������� �������.</p>
<p>��������� ��� � ������ ����������� �������������� - 2,40 ���. <br>��������� ��� ��� ����� ����������� - 2.60 ���.</p><p>
��� ������������� � ��� ����������� ����� ����������� ���������� ������������ � ����� ���������� ������� Pir Company.</p>
<p><b>��������� ��������.</b><br>
�������� ������:<br>
1. ����� �������� ����������� �� ������� ����������� Viber.<br>
2. ���� ���������� Viber �������, ������������ Viber ���������.<br>
3. ���� ���������� �� ���������, �������� ������������ sms-���������.<br><br>
����� Viber-��������� �� 1 ���. ��������.<br>
���� ����������� ������������� �������� � ������ ����������.<br>
C�������� ������-��������� 1,90 ���., ��������� ���-��������� 2,40
��������� ��-��������� 0,50, ��������� ���-��������� 2,40</p>
';

?>